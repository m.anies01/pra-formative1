import java.util.*; // mengimport (mengambil) utilitis (kakas/peralatan) dari library java
                    // tanda bintang artinya mengambil semua fungsi yang ada di library java
                    // yang berlokasi di java.util.

public class Task31 { // membuat kelas java dengan nama Task31
  public static void main(String args[]) { // Membuat method void main untuk eksekusi program java
    ArrayList<String> list = new ArrayList<String>(); // Menginstansi/membuat objek baru dari collection ArrayList dengan tipe data String
                                                    // dengan nama 'list'
    list.add("Mango"); // menambahkan data String "Mango" ke dalam objek list dengan method .add()
    list.add("Apple"); // menambahkan data String "Apple" ke dalam objek list dengan method .add()
    list.add("Banana"); // menambahkan data String "Banana" ke dalam objek list dengan method .add()
    list.add("Grapes"); // menambahkan data String "Grapes" ke dalam objek list dengan method .add()
    // Iterator : salah satu interface yang tersedia di dalam library
    Iterator itr = list.iterator(); // Menginstansi/membuat objek Iterator dengan nama itr yang kemudian
                                    // diisi dengan objek list diikuti method iterator()
                                    // dari collection ArrayList
    while (itr.hasNext()) { // hasNext digunakan untuk mengecek apakah itr 
                            // masih memiliki nilai pada element selanjutnya atau tidak
                            //  
      System.out.println(itr.next()); // mengambil nilai elemen selanjutnya pada itr
    }
  }
}

public class Task32 { // membuat kelas java dengan nama Task31
    public static void main(String args[]) { // Membuat method void main untuk eksekusi program java
      ArrayList<String> list = new ArrayList<String>(); // Menginstansi/membuat objek baru dari collection ArrayList dengan tipe data String
                                                        // dengan nama 'list'
      list.add("Mango"); // menambahkan data Mango ke dalam element ArrayList yang sudah dinstasiasi menjadi objek list
      list.add("Apple"); // menambahkan data Apple ke dalam element ArrayList yang sudah dinstasiasi menjadi objek list
      list.add("Banana"); // menambahkan data Banana ke dalam element ArrayList yang sudah dinstasiasi menjadi objek list
      list.add("Grapes"); // menambahkan data Grapes ke dalam element ArrayList yang sudah dinstasiasi menjadi objek list
      System.out.println(list); // menampilkan data element ArrayList yang terdapat di objek list
    }
  }
  /// Output : [Mango, Apple, Banana, Grapes]
  
  /// =================== PERBEDAAN ===================
  // Perbedaan antara Task 31 dengan Task 32 adalah dalam hal mengambil/menampilkan element pada
  // ArrayList yang sudah di instansiasi ke dalam objek list
  // dalam Task31 element ArrayList diambil dengan cara mengecek element selanjutnya
  // jika masih ada nilai element selanjutnya, akan true dan kembali melakukan perulangan
  // Dalam task31 mengambil setiap element dari ArrayList satu-satu kemudian ditampilkan
  // sehingga hasilnya ditampilkan dalam bentuk stack (tumpukan)
  // Sedangkan dalam task32, setiap data string dimasukkan ke dalam ArrayList
  // list kemudian baru ditampilkan semua elemen data yang ada pada
  // ArrayList tersebut dengan memanggil instansiasi objek ArrayList tersebut
  // yaitu list