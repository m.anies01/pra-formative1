public class Task1 { // membuat kelas java dengan nama Task1 dan dengan modifier public
                    // agar bisa diakses oleh kelas lain dalam satu folder
                    // nama kelas dalam java harus sama dengan nama file-nya
    public static void main(String[] args) { // method utama tempat menjalankan program utama dari java
      int i = 1; // melakukan inisialisasi, memasukkan nilai 1 ke dalam variabel i
                // sehingga variabel i bernilai 1
      do { // melakukan perintah eksekusi program dengan nilai i awal 1 seperti yang sudah diisikan
        // pada variabel di atas
        if (i == 5) { // melakukan pengecekan, apakah nilai dalam variabel i sama dengan 5?
                    // jika benar, maka lanjut ke eksekusi perintah didalam kondisi ini
          i++; // jika benar nilai variabel i sama dengan 5, maka lakukan penambahan nilai di dalam variabel i dengan nilai 1
          break; // kemudian eksekusi perintah break, yaitu dengan meng-cut (memotong) aliran eksekusi
                // perulangan tanpa melakukan eksekusi pada perintah (algoritma) berikutnya di dalam looping
                // namun mengeksekusi perintah yang ada di bawah/diluar looping
        }
        System.out.println(i); // selama kondisi di atas bernilai false dan nilai dalam variabel i masih dalam range (rentang)
                                // kurang dari sama dengan 10, maka cetak (print) nilai dari variabel i tersebut 
                                // dalam satu baris (printline) dan beri enter diakhir
        i++; // lakukan penambahan nilai pada variabel i dengan nilai 1
      } while (i <= 10); // cek apakah nilai i kurang dari sama dengan 10, jika benar (true), maka kembali
                        // ke perintah di atas dan eksekusi perulangan. Namun jika berada diluar range (rentang)
                        // nilai tersebut, maka hentikan program.
                        // namun dalam kasus ini, program sudah dihentikan oleh perintah break ketika nilai i
                        // sama dengan 5 seteh dilakukan increment (penambahan) nilai i secara teratur
                        // dengan jenis increment postfix (penambahan dilakukan diakhir).
    }
  }

  // Program di atas jika dieksekusi akan menghasilkan output sebagai berikut:
  // 1
  // 2
  // 3
  // 4