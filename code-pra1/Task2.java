public class Task2 { // membuat kelas java dengan nama Task2 dan dengan modifier public
                    // agar bisa diakses oleh kelas lain dalam satu folder
                    // nama kelas dalam java harus sama dengan nama file-nya
    public static void main(String[] args) { // method utama tempat menjalankan program utama dari java
        int age = 25; // Memasukkan nilai 25 ke dalam variabel age yang bertipe data integer
        int weight = 48; // Memasukkan nilai 48 ke dalam variabel weight yang bertipe data integer
        if (age >= 18) { // melakukan pengecekan apakah variabel age bernilai lebih dari sama dengan 18?
                        // jika benar, maka lakukan eksekusi perintah selanjutnya yang berada di dalam kondisi ini
          if (weight > 50) { // Tahap ini melakukan pengecekan nilai dari variabel weight apakah lebih besar dari 50
                            // namun pengecekan dilakukan apabila nilai variabel age bernilai lebih 
                            // dari sama dengan 18 (true)
            System.out.println("You are eligible to donate blood"); // jika benar, maka cetak kalimat ini
          } else { // namun jika  nilai age lebih dari sama dengan 18 namun nilai weight tidak lebih dari 50 (false)
                    // maka eksekusi perintah berikut
            System.out.println("You are not eligible to donate blood"); //  melakukan cetak kalimat yang berada dalam tanda kutip
          }
        } else { // perintah ini dieksekusi ketika nilai dalam variabel age tidak lebih dari 18 (false)
          System.out.println("Age must be greater than 18"); // cetak kalimat dalam tanda kutip
        }
      }
}
// Output :
// You are not eligible to donate blood