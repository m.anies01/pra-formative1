import java.util.*; // import (mengambil) semua fungsi/interface yang berada
                    // di dalam library java.util

public class Task4 { // Membuat kelas java dengan nama Task4
    public static void main(String[] args) { // membuat method static void dengan nama main
       Scanner inputNilai = new Scanner(System.in);// membuat fungsi scanner dengan nama  objek inputNilai
                                                // System.in --> mengambil masukan dari sistem (apapun os-nya)
                                                // untuk dimasukkan ke dalam program
       
       System.out.print("Masukkan nilai pertama untuk ditambah: "); // Menampilkan kalimat dalam tanda kutip
                                                                    // ke dalam commandline tanpa spasi di belakangnya
       int a = inputNilai.nextInt(); // Memasukkan hasil inputan integer inputNilai ke dalam variabel integer a
       System.out.print("Masukkan nilai kedua untuk ditambah: ");// Menampilkan kalimat dalam tanda kutip
                                                                // ke dalam commandline tanpa spasi di belakangnya
       int b = inputNilai.nextInt(); // Memasukkan hasil inputan integer inputNilai ke dalam variabel integer b

       System.out.printf("%d "+"+"+" %d"+" =" + " %d\n",a,b,(a+b)); // mengeluarkan nilai variabel a dan b dari yang sudah
                                                                    // dilakukan inputan kemudian melakukan operasi penambahan
                                                                    // nilai pada variabel a dan b kemudian menampilkannya
    }
}
