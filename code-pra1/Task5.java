import java.util.*; // Mengambil semua fungsi dari library java yang berada di package java.util

public class Task5{ // Membuat kelas java dengan nama HasMapExample1
 public static void main(String args[]){ // membuat method static void dengan nama main
   HashMap<Integer,String> map=new HashMap<Integer,String>(); // Membuat dari HashMap dengan nama map
                                                              // dengan parameter masukan key data berupa Integer 
                                                              // kemudian masukan value-nya berupa data string
   map.put(1,"Mango"); // Memasukkan key integer 1 dan value string "mango" ke dalam objek HashMap map
   map.put(2,"Apple"); // Memasukkan key integer 2 dan value string "Apple" ke dalam objek HashMap map
   map.put(3,"Banana"); // Memasukkan key integer 3 dan value string "Banana" ke dalam objek HashMap map
   map.put(4,"Grapes"); // Memasukkan key integer 4 dan value string "Grapes" ke dalam objek HashMap map
   map.put("pisang","Pisang"); // Memasukkan key data string "pisang" dan value string "Pisang" ke dalam objek HashMap map

   System.out.println("Iterating Hashmap..."); // Menampilkan kata dalam tanda kutip dan memberikan enter di akhir kata
   // Perulangan for-each untuk menampilkan key dan value dari HashMap yang sudah dibuat diatas
   for(Map.Entry m : map.entrySet()){ // Memasukan semua data dari objek HashMap map ke dalam m Map.Entry
    System.out.println(m.getKey()+" "+m.getValue()); // Mengambil nilai key dari hashMap m dengan m.getKey
                                                    // dan mengambil nilai value dari hashMap m dengan m.getValue
   }
  }
}
/// ============== OUTPUT PROGRAM ==============
// Program diatas akan eror jika dijalanakan karena terdapat satu kesalahan input key dan value pada
// objek hashMap map yaitu key: pisang dan value Pisang
// Hal tersebut terjadi karena key dan value yang dimasukkan berupa string semua
// Sedangkan dalam pembuatan objek hashMap di atas mendefinisikan inputan key dan value
// dengan aturan masukan data key yang berupa integer dan data value berupa string
// maka agar tidak terjadi eror, harus dilakukan perubahan nilai key string dan value string menjadi
// key integer dan value string sesuai dengan format data diatasnya
// seperti ini : map.put(5,"Pisang");
